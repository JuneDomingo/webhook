'use strict';

const express = require('express');
const exec = require('child_process').exec;
const config = require('./config');

const app = express();
const tasks = config.tasks;
const port = config.port || 3005;

function register(task) {
	app.post(`/${task.name}`, (req, res) => {
		if (task.token && !req.query.token) {
			return res.end("No token provided.");
		}

		if (req.query.token && req.query.token !== task.token) {
			return res.end("Token din't match!");
		}

		exec(task.command, (err, stdout, stderr) => {
			console.log(`stderr: ${stderr}`);
			res.end(`Task ${task.name} done!`);
		});
	 });
}

for (const task of tasks) {
	register(task);
}

app.listen(port, () => {
	console.log(`Webhook task manager listening on port ${port}`);
});